import React from 'react'
import {Modal, Button, Row, Col, Container, Card} from 'react-bootstrap'
import moment from 'moment'
import axios from 'axios'
import Swal from 'sweetalert2'
import { useNavigate } from 'react-router-dom'


const RecordDetails = ({show, onHide, data, dateDifference, updateTheState}) => {
  
  const navigate = useNavigate();
  // Start: Functions
  function capitalizeFirstLetter(str) {
    return str.replace(/\b\w/g, char => char.toUpperCase());
  }

  function handlePrint(){
    const printWindow = window.open('', '_blank');
    if (printWindow) {
      const printContents = document.getElementById('main-report').innerHTML;
      //This is the html page that will be printed
      printWindow.document.write(`
      <html>
      <head>
        <title>Generated with Comtech Accounting System | Created by: CLARK</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
        <style>
          h4 {
            font-size: 20px !important;
            text-transform: uppercase
          }
          h5, .h5, .main-dates {
            font-size: 16px !important;
          }
          .main-table {
            max-width: 500px !important;
          }
        </style>
      </head>
      <body style="font-size: 14px !important">
        <div class="mx-3 text-center" >
          <img src="/img/logo.png" width="300px"/>
        </div>

        ${printContents}
        
        <div class="mt-4 mx-3 text-center h5">
        Prepared by: ________________________________
        </div>
      </body>
      </html>
      `);
      
      printWindow.document.close();
      
      // Set up the onafterprint event handler to close the tab/window when print dialog is canceled.
      const afterPrintHandler = () => {
        printWindow.close();
      };

      printWindow.onafterprint = afterPrintHandler;

      // Print the document
      printWindow.print();

    }
  };

  function handleArchivingReport(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You can restore this if you want in the archive folders.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, archive it!'
    }).then((result) => {
      if (result.isConfirmed) {
        const url = `http://localhost:4000/bk/api/expenses/${data._id}/archive`;
        axios.put(url)
        .then(result => {
          onHide() //to hide the modal
          updateTheState() //to update the list
          Swal.fire({
            title: 'Record archived successfully!',
            icon: 'success',
            confirmButtonColor: "#2c3e50",
          })
        })
      }
    })
  }
  
  function handleEditingReport(){
    Swal.fire({
      title: 'Are you sure?',
      text: "Updating this report might change the accuracy of the data. Do this unless it is necessary.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then(result => {
      if(result.isConfirmed){
        navigate(`/edit-record/${data._id}`)
      }
    });
  }

  function handleRestoringReport(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to restore this record.",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, restore it!'
    }).then((result) => {
      if (result.isConfirmed) {
        const url = `http://localhost:4000/bk/api/expenses/${data._id}/restore`;
        axios.put(url)
        .then(result => {
          onHide() // to hide the modal
          updateTheState() // to update the list
          Swal.fire({
            title: 'Record restored successfully!',
            icon: 'success',
            confirmButtonColor: "#2c3e50",
          })
        })
      }
    })
  }

  // End: Functions

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      // animation={false}
      backdrop="static"
    >
      <Modal.Header closeButton className='bg-light text-dark'>
        <Modal.Title id="contained-modal-title-center">
          <div className='d-flex gap-5'>
            <div>
              Record #: <strong>{data.reportId.toString().padStart(4, '0')}</strong>
            </div>
            <div>
              Reporting Dates: <strong>{moment(data.startDate, "YYYY-MM-DD").format('MMM DD, YYYY')} - {moment(data.endDate, "YYYY-MM-DD").format('MMM DD, YYYY')}</strong>
            </div>
          </div>
        </Modal.Title>
      </Modal.Header>

      <Modal.Body className='bg-white'>
       
        {!data.isActive ? 
          <div className='delete-btn'><h3 className='text-center text-white'>Archived!</h3></div>
        :
        <></>
        }
        
        <div id='main-report'>
          <h4 className='text-center my-3'>Income Statement</h4>

          {/* START: DATES */}
          <p className='mb-2 main-dates text-center'>
            <span className='me-5'>Start Date: {moment(data.startDate, "YYYY-MM-DD").format('MMMM DD, YYYY')}</span><span className='me-5'>End Date: {moment(data.endDate, "YYYY-MM-DD").format('MMMM DD, YYYY')}</span><span>Reporting Period: {dateDifference == 1 ? dateDifference + ' day' : dateDifference + ' days'}</span>
          </p>
          {/* END: DATES */}
          <Container className='px-5 pt-2 main-table border-top border-dark border-bottom'>


            {/* REVENUE */}
            <Row>
              {/* <div>
                <p className='border-bottom border-dark mb-2'></p>
              </div> */}
              <Col xs={6}>
                <p className='fst-italic mb-1 text-end'>REVENUE</p>
              </Col>
            </Row>  
            <Row>
            {/* END: REVENUE */}

            {/* START: SERVICE INCOME */}
              <Col xs={6}>
                <p className='mb-1 '>Service Income</p>
              </Col>
              <Col className='text-end'>
                <span>{data.serviceIncome.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
              </Col>
              <Col></Col>
            </Row>  
            {/* END: SERVICE INCOME */}

            {/* START: SALES INCOME */}
            <Row>
              <Col xs={6}>
                <p className='mb-1'>Sales Income</p>
              </Col>
              <Col className='text-end'>
                <span>{data.salesIncome.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
              </Col>
              <Col></Col>
            </Row>
            {/* END: SALES INCOME */}

            {/* START: OTHER INCOME */}
            <Row>
              <Col xs={6}>
                <p className='mb-1'>Other Income</p>
              </Col>
              <Col className='border-bottom border-dark text-end'>
                  <span>{data.otherIncome.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
              </Col>
              <Col>
              </Col>
            </Row>
            {/* END: OTHER INCOME */}
            
            {/* START: TOTAL REVENUE */}
            <Row className='mt-2 mb-3'>
              <Col xs={6}>
                <p className='mb-1 fw-bold'>TOTAL REVENUE:</p>
              </Col>
              <Col>
              </Col>
              <Col className='text-end'>
                <span >₱ {data.revenue.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
              </Col>
            </Row> 
            {/* END: TOTAL REVENUE */}

            {/* START: EXPENSES */}
            <Row >
              <Col xs={6}>
                <p className='fst-italic mb-1 text-end'>EXPENSES</p>
              </Col>
            </Row> 
            
              {data.expenses.map(item => {
                return <>
                <Row key={item._id}>
                  <Col xs={6}>
                    <p className='mb-1'>{capitalizeFirstLetter(item.expense)}</p>
                  </Col>
                  <Col className='text-end'>
                    <span >{item.amount.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
                  </Col>
                  <Col className='text-end'>
                  
                  </Col>
                </Row>
                </>
              })}

            {/* END: EXPENSES */}
            
            {/* START: MIDDLE BORDER TOP */}
            <Row>
              <Col xs={6}>
              </Col>
              <Col  className='border-top border-dark'>
              </Col>
              <Col>
              </Col>
            </Row>
            {/* END: MIDDLE BORDER TOP */}
            
            {/* START: TOTAL EXPENSES */}
            <Row className='mt-2'>
              <Col xs={6}>
                <p className='mb-1 fw-bold'>TOTAL EXPENSES:</p>
              </Col>
              <Col>
              </Col>
              <Col className='text-end '>
                <span >₱ {data.totalExpenses.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
              </Col>
              
              {/* <div>
                <p className='border-bottom border-dark pb-2'></p>
              </div> */}
            </Row> 
            {/* END: TOTAL EXPENSES */}
              
            {/* START: NET INCOME */}
            <Row className='mb-3'>
              <Col xs={9}>
              <p className='mb-1 text-start fw-bold'>NET INCOME:</p>
              </Col>
              <Col className='text-end fw-bold'>
                <span >₱ {data.profit.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
              </Col>
            </Row>   
            {/* END: NET INCOME */}
          </Container>

          {/* START: SOURCE JOB ORDER */}
          <Container className='mt-3'>
            <h5 className='d-inline me-2'>Source Job Order/s:</h5>
            <span className='h5 fw-bold border px-2 rounded-2'>{data.jobOrders == "" ? "N/A" : data.jobOrders}</span>
          </Container>
          {/* END: SOURCE JOB ORDER */}

          {/* START: REMARKS */}
          <Container className='mt-3 mb-3'>
            <h5>Remarks:</h5>
            <Card className='rounded-0 border-1 border-dark-subtle p-0'>
              <Card.Body className='text-dark bg-white p-0'>
                <p className='p-1 px-2'>
                  {data.remarks == "" ? "N/A" : data.remarks}
                </p>
              </Card.Body>
            </Card>
          </Container>
          {/* END: REMARKS */}
        </div>
        
      </Modal.Body>

      {/* START: FOOTER BUTTONS */}
      <Modal.Footer className='bg-light '>
        {
          data.isActive === true ? 
            <>
              <Button size="sm" className='delete-btn me-auto' onClick={handleArchivingReport}><i class="fa-regular fa-trash-can"></i> Archive</Button>
              <Button size="sm" className='edit-btn' onClick={handleEditingReport}><i class="fa-solid fa-pen-to-square"></i> Edit Record</Button>
              <Button size="sm" className='print-btn' onClick={handlePrint}><i class="fa-solid fa-print"></i> Print</Button>
            </>
          :
            <Button size="sm" className='restore-btn ms-auto' onClick={handleRestoringReport}><i class="fa-solid fa-trash-can-arrow-up"></i> Restore</Button>
        }
      </Modal.Footer>
      {/* END: FOOTER BUTTONS */}
    </Modal>
  )
}

export default RecordDetails