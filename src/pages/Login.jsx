import React from 'react'
import {Card, Form, Button} from 'react-bootstrap'

const Login = () => {
  return (
    <div className='vh-100 bg-dark text-light d-flex flex-column align-items-center justify-content-center'>
        <Card bg={"light"} border='light'>
          <Card.Body className=''>
            <h3 className='text-success text-center mb-3 fw-bold'>COMTECH ACCOUNTING <br/> SYSTEM</h3>
            <Form>

              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label className='text-dark'>Username</Form.Label>
                <Form.Control type="email" required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                <Form.Label className='text-dark'>Password</Form.Label>
                <Form.Control type="password" required/>
              </Form.Group>

              <div className='text-center'>
                <Button variant='primary py-1 rounded-1' size='' type='submit'>Login</Button>
              </div>
              
            </Form>
          </Card.Body>
        </Card>
    </div>
  )
}

export default Login