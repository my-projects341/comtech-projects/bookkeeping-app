import React from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Sidebar = () => {
  return (
    <div className='vh-100 bg-dark p-3 sticky-top'>
            <ul className="list-unstyled mt-3">
                <li className='mb-4 d-flex flex-column'>
                    <Link to="/add-record" className='text-decoration-none fw-bold bg-primary rounded-5 text-light px-3 py-2 link-dark d-flex justify-content-center align-items-center gap-2 hover-up'><span className='fs-4'><i class="fa-solid fa-circle-plus"></i></span><span className='line'>ADD NEW RECORD</span></Link>
                </li>

                <Link to={'/'} className='text-decoration-none fw-bold link-light'>
                    <li className='mb-3 border-bottom border-2 pb-2'>
                        <i class="fa-solid fa-chart-line"></i><span className='ms-2'>DASHBOARD</span>
                    </li>
                </Link>
                <Link to={"/all-records"} className='text-decoration-none fw-bold link-light'>
                    <li className='mb-3 border-bottom border-2 pb-2'>
                        <i class="fa-solid fa-book"></i><span className='ms-2'>ALL RECORDS</span>
                    </li>
                </Link>
                <Link to={"/search"} className='text-decoration-none fw-bold link-light'>
                    <li className='mb-3 border-bottom border-2 pb-2'>
                        <i class="fa-solid fa-search"></i><span className='ms-2'>SEARCH</span>
                    </li>
                </Link>
                <Link to={"/generate-report"} className='text-decoration-none fw-bold link-light'>
                    <li className='mb-3 border-bottom border-2 pb-2'>
                        <i class="fa-solid fa-print"></i><span className='ms-2'>GENERATE REPORT</span>
                    </li>
                </Link>
                <Link to={"/archived-records"} className='text-decoration-none fw-bold link-light'>
                    <li className='mb-3 border-bottom border-2 pb-2'>
                    <i class="fa-solid fa-trash"></i><span className='ms-2'>ARCHIVE</span>
                    </li>
                </Link>
        
            </ul>
    </div>
  )
}

export default Sidebar