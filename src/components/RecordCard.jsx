import React, { useState } from 'react'
import {Card, Button, Modal, Row, Col} from 'react-bootstrap'
import RecordDetails from './RecordDetails'
import moment from 'moment'


const RecordCard = ({data, updateTheState}) => {

  const [modalShow, setModalShow] = React.useState(false);

  //Start Functions
  const dateDifference = moment(data.endDate).diff(moment(data.startDate), 'days')
  const formatDate = (date) => {
    return moment(date, "YYYY-MM-DD").format('MMM DD, YYYY')
  }
  const formatCurr = (curr) => {
    return "₱ " + curr.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })
  }
  //End Functions

  return (
    <>
    <Card bg={'white'} as={'a'} border='1 border-secondary rounded-0 text-dark text-decoration-none' className='mb-1  card-hover' onClick={() => setModalShow(true)}>
        <Card.Body className='py-1'>
          <Row>
            <Col xs={2} className='d-flex gap-1 align-items-center'>
            <strong>Record #: </strong><span>{data.reportId.toString().padStart(4, '0')}</span>
            </Col>
            <Col xs={4} className=''>
            <strong>Report Period:</strong><br /><span>{formatDate(data.startDate)} - {formatDate(data.endDate)} ({dateDifference == 1 ?dateDifference + ' day' : dateDifference + ' days'})</span>
            </Col>
            <Col xs={2}>
            <strong>Revenue:</strong><br /> <span>{formatCurr(data.revenue)}</span>
            </Col>
            <Col xs={2}>
            <strong>Expenses:</strong><br /> <span>({formatCurr(data.totalExpenses)})</span>
            </Col>
            <Col xs={2}>
            <strong>Profit:</strong><br /> <span>{formatCurr(data.profit)}</span>
            </Col>
          </Row>
        </Card.Body>
    </Card>
    <RecordDetails 
        show={modalShow}
        onHide={() => setModalShow(false)}
        dateDifference={dateDifference}
        data={data}
        updateTheState={updateTheState}
    />
    </>
  )
}

export default RecordCard