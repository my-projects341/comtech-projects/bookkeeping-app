import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import "bootswatch/dist/solar/bootstrap.min.css";
import './App.css'
import {Row, Col} from 'react-bootstrap'
import NavigationBar from './components/NavigationBar'
import Sidebar from './components/Sidebar';

// Pages
import NotFound from './pages/NotFound';
import Dashboard from './pages/Dashboard';
import NewRecord from './pages/NewRecord';
import Login from './pages/Login';
import AllRecords from './pages/AllRecords';
import EditRecord from './pages/EditRecord';
import ArchivedRecords from './pages/ArchivedRecords';
import SearchRecords from './pages/SearchRecords';

function App() {

  return (
    <>
    <Router>
      <Routes>

          <Route path='/login' element={<Login/>} />

          {/* Main Pages */}
          <Route path='/' element={
            <>
              <Row className='g-0'>
                  <NavigationBar />
                  <Col xs={3} lg={2}>
                    <Sidebar />
                  </Col> 
                  <Col xs={9} lg={10}>
                    {/* Page here */}
                    <Dashboard />
                  </Col>
                </Row>
            </>
          } />

          <Route path='/add-record' element={
            <>
              <Row className='g-0'>
                  <NavigationBar />
                  <Col xs={3} lg={2}>
                    <Sidebar />
                  </Col> 
                  <Col xs={9} lg={10}>
                    {/* Page here */}
                    <NewRecord />
                  </Col>
                </Row>
            </>
          } />
          
          <Route path='/edit-record/:id' element={
            <>
              <Row className='g-0'>
                  <NavigationBar />
                  <Col xs={3} lg={2}>
                    <Sidebar />
                  </Col> 
                  <Col xs={9} lg={10}>
                    {/* Page here */}
                    <EditRecord />
                  </Col>
                </Row>
            </>
          } />
          
          <Route path='/all-records' element={
            <>
              <Row className='g-0'>
                  <NavigationBar />
                  <Col xs={3} lg={2}>
                    <Sidebar />
                  </Col> 
                  <Col xs={9} lg={10}>
                    {/* Page here */}
                    <AllRecords />
                  </Col>
                </Row>
            </>
          } />
          
          <Route path='/search' element={
            <>
              <Row className='g-0'>
                  <NavigationBar />
                  <Col xs={3} lg={2}>
                    <Sidebar />
                  </Col> 
                  <Col xs={9} lg={10}>
                    {/* Page here */}
                    <SearchRecords />
                  </Col>
                </Row>
            </>
          } />

          <Route path='/archived-records' element={
            <>
              <Row className='g-0'>
                  <NavigationBar />
                  <Col xs={3} lg={2}>
                    <Sidebar />
                  </Col> 
                  <Col xs={9} lg={10}>
                    {/* Page here */}
                    <ArchivedRecords />
                  </Col>
                </Row>
            </>
          } />

          <Route path='*' element={<NotFound/>} />
        </Routes>
      </Router>
    </>
  )
}

export default App
