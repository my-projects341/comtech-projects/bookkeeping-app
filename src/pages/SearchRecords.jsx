import React, { useEffect, useState } from 'react'
import RecordCard from '../components/RecordCard';
import axios from 'axios';
import { Button, Form, InputGroup, Spinner } from 'react-bootstrap';
import { useSearchParams } from 'react-router-dom';

const SearchRecords = () => {
    const [records, setRecords] = useState([]);
    const [loadMore, setLoadMore] = useState(10)
    const [isLoading, setIsLoading] = useState(false)
    const [search, setSearch] = useState('')

    //To update the ArchiveRecord component when there are state changes on its child components
    const [updateState, setUpdateState] = useState(false)
    function updateTheState() {
        if(updateState === false) {
            setUpdateState(true);
        } else {
            setUpdateState(false)
        }
    }
  console.log('isLoading', isLoading)
    const handleSearch = () => {

      setIsLoading(true)

      setTimeout(() => {
        const url = `http://localhost:4000/bk/api/expenses/search?q=${search}`;
        axios.get(url).then(data => {
            setRecords(data.data)
            setIsLoading(false)
        })
      }, 500)
    }

    useEffect(() => {
      setIsLoading(true)
      setTimeout(() => {
        handleSearch()
        setIsLoading(false)
      }, 500)
    }, [])
    
    console.log(search)
  return (
    <> 
    <h2 className='px-4 pt-4 mb-4 fw-bold text-dark'>SEARCH RECORDS</h2>
    {/* <p className='px-5'>Input the necessary information for the day's accounting. Always double check before saving to ensure accurate record.</p> */}

    <div className='ps-5'>
      <InputGroup className="align-items-center mb-4 gap-0 w-50" >
        <h5 className='mb-0 me-2'><i class="fa-solid fa-search"></i> Search Records:</h5>
          <Form.Control
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                  handleSearch()
                }
          }}
          placeholder="input record #"
          className='align-self-center border-secondary'
          />
          <Button className='align-self-center' onClick={e => handleSearch()} variant="outline-secondary rounded-0" id="button-addon2">
          <i className="fa-solid fa-magnifying-glass"></i>
          </Button>
      </InputGroup>
    </div>


    {isLoading === true ? 
      <div className='text-center my-5'>
        <Spinner animation="border" variant="primary" size='lg'/>
        <h4 className='mt-2'>Loading...</h4>
      </div>
      :
     records.length !== 0 ?
      
      <div>
        <div className='mx-5 px-5 rounded-3'>
            {
              records.slice(0, loadMore).map(record => {
                return <RecordCard key={record._id} data={record} updateTheState={updateTheState}/>
              })
            }
        </div>
        <div className='text-center mt-3 mb-5'>
          {
            records.length <= loadMore ?
              <span>-- End of List --</span>
            :
            <Button variant='dark' size='sm' onClick={() => setLoadMore(loadMore + 10)}>Load More</Button>
          }
        </div>
      </div>
      :
      <h4 className='text-center my-5'>No records found.</h4>
    }

    </>
  )
}

export default SearchRecords