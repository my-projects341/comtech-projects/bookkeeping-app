import React, { useEffect, useState } from 'react'
import RecordCard from '../components/RecordCard';
import axios from 'axios';
import { Button, Spinner } from 'react-bootstrap';

const AllRecords = () => {
    const [records, setRecords] = useState([]);
    const [loadMore, setLoadMore] = useState(10)
    const [isLoading, setIsLoading] = useState(true)

    //To update the ArchiveRecord component when there are state changes on its child components
    const [updateState, setUpdateState] = useState(false)
    function updateTheState() {
        if(updateState === false) {
            setUpdateState(true);
        } else {
            setUpdateState(false)
        }
    }

    useEffect(() => {
        const url = 'http://localhost:4000/bk/api/expenses';
        axios.get(url).then(data => {
            setRecords(data.data)
            setIsLoading(false)
        })
    }, [updateState])
    
    console.log(records)
  return (
    <>
    <h2 className='px-4 pt-4 mb-3 fw-bold text-dark'>ALL RECORDS</h2>
    {/* <p className='px-5'>Input the necessary information for the day's accounting. Always double check before saving to ensure accurate record.</p> */}
    <h5 className='px-5 mb-4'><i class="fa-solid fa-filter"></i> Filter:</h5>

    {records.length !== 0 ? (isLoading === true ?
      <div className='text-center my-5'>
        <Spinner animation="border" variant="primary" size='lg'/>
        <h4 className='mt-2'>Loading...</h4>
      </div>
      :
      <div>
        <div className='mx-5 px-5 rounded-3'>
            {
              records.slice(0, loadMore).map(record => {
                return <RecordCard key={record._id} data={record} updateTheState={updateTheState}/>
              })
            }
        </div>
        <div className='text-center mt-3 mb-5'>
          {
            records.length <= loadMore ?
              <span>-- End of List --</span>
            :
            <Button variant='dark' size='sm' onClick={() => setLoadMore(loadMore + 10)}>Load More</Button>
          }
        </div>
      </div>)
      :
      <h4 className='text-center my-5'>No records.</h4>
    }

    </>
  )
}

export default AllRecords