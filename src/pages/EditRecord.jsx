import {useState, useEffect} from 'react'
import {Container, Row, Col, Form, InputGroup, Button, Table} from 'react-bootstrap'
import axios from 'axios'
import { useNavigate, useParams, Link } from 'react-router-dom'
import Swal from 'sweetalert2'

const EditRecord = () => {
  const {id} = useParams()
  const navigate = useNavigate()
  const [reportId, setReportId] = useState(0)
  const [startDate ,setStartDate] = useState('')
  const [endDate ,setEndDate] = useState('')
  const [serviceIncome, setServiceIncome] = useState(0)
  const [salesIncome, setSalesIncome] = useState(0)
  const [otherIncome, setOtherIncome] = useState(0)
  const [jobOrders, setJobOrders] = useState('')
  const [selectedCell, setSelectedCell] = useState(null);
  const [data, setData] = useState([
    ['', 'ITEM', 'AMOUNT (₱)', ],
    ['1', '', ''],
    ['2', '', ''],
    ['3', '', ''],
  ]);
  const [remarks, setRemarks] = useState('')
  const revenue = parseFloat(serviceIncome == "" ? 0 : serviceIncome) + parseFloat(salesIncome == "" ? 0 : salesIncome) + parseFloat(otherIncome == "" ? 0 : otherIncome);
  const expenses = data.slice(1).map((row) => isNaN(parseInt(row[2])) ? 0 : parseInt(row[2])).reduce((x, y) => x + y);
  const netIncome = revenue - expenses
  
  

  // START LOGIC FUNCTIONS
  // Function to handle the beforeunload event
  const handleBeforeUnload = (event) => {
    event.preventDefault();
    // Set the message to display when the user tries to leave the page
    event.returnValue = 'Are you sure you want to leave? Your form data will be lost.';
  };

  // To convert the expenses object array to a 2 dimentional array
  const expensesTo2DArray = (expenses) => {
    // Create the initial 2D array with header row
    const resultArray = [['', 'ITEM', 'AMOUNT (₱)']];
  
    // Loop through the expenses and add rows to the result array
    expenses.forEach((expense) => {
      const { row, expense: item, amount } = expense;
      resultArray.push([row.toString(), item, amount.toString()]);
    });
  
    return resultArray;
  };

  useEffect(() => {
    // Add the event listener when the component mounts
    window.addEventListener('beforeunload', handleBeforeUnload);

    //Retrive userinfo
    const url = `http://localhost:4000/bk/api/expenses/${id}`;
    axios.get(url)
    .then(result => {
      const record = result.data;
      // console.log("result from fetching record data: ")
      // console.log(record)
      setStartDate(record.startDate);
      setEndDate(record.endDate);
      setServiceIncome(record.serviceIncome);
      setSalesIncome(record.salesIncome);
      setOtherIncome(record.otherIncome);
      setJobOrders(record.jobOrders);
      setRemarks(record.remarks);
      setReportId(record.reportId)
      setData(expensesTo2DArray(record.expenses));
    })
    .catch(err => console.log('error fetch record data: ', err))

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, [])

  const handleUpdate = (e) => {
    e.preventDefault()
    // Prepare the row data for the table instance
    const rowData = data.slice(1).map(row => {
      if(row[1].length !== 0 || row[2].length !== 0){
        const data = {
          row: row[0],
          expense: row[1],
          amount: Number(row[2]),
        }
        return data
      }
    }).filter(Boolean);
    // console.log('rowData', rowData)
    
    // Make the API call to save the table instance
    const url = `http://localhost:4000/bk/api/expenses/${id}/update`;
    axios.put(url, {
      startDate: startDate,
      endDate: endDate,
      serviceIncome: serviceIncome,
      salesIncome: salesIncome,
      otherIncome: otherIncome,
      jobOrders: jobOrders,
      expenses: rowData,
      remarks: remarks
    }, {
      headers: {
        'Content-Type': 'application/json',
      }
    })
      .then((response) => {
        Swal.fire({
					title: `Record #${reportId.toString().padStart(4, '0')} was updated successfully!`,
					icon: 'success',
					confirmButtonColor: "#2c3e50",
				})
        navigate('/all-records')
      })
      .catch((error) => {
        console.error('Error saving table instance:', error);
        Swal.fire({
					title: 'There is an error saving the record!',
					icon: 'error',
					confirmButtonColor: "#2c3e50",
				})

      });

  };

  const handleCellClick = (row, col) => {
    if (row === 0 || col === 0) return;
    setSelectedCell({ row, col, editing: true });
  };

  const handleCellChange = (event, row, col) => {
    if (row === 0 || col === 0) return;

    const newData = [...data];
    newData[row][col] = event.target.value;

    setData(newData);
  };

  const handleAddRow = () => {
    const lastRowIndex = data.length - 1;
    const newNumber = parseInt(data[lastRowIndex][0]) + 1;
    const newRow = [newNumber.toString(), '', ''];
    setData((prevData) => [...prevData, newRow]);
  };

  const handleBlur = () => {
    setSelectedCell(null)
  };

  const handleKeyDown = (event, row, col) => {

    if (event.key === 'Enter' || event.key === 'Tab') {
      event.preventDefault();
      const nextRow = col === 2 ? row + 1 : row;
      const nextCol = col === 2 ? 1 : col + 1;
      setSelectedCell({ row: nextRow, col: nextCol, editing: true });
    }


  };

  const renderCell = (row, col, value) => {
    const isSelected =
      selectedCell && selectedCell.row === row && selectedCell.col === col;
    const isEditing = isSelected && selectedCell.editing;

    return (
      <td
        key={`${row}-${col}`}
        onClick={() => handleCellClick(row, col)}
        className={`${isSelected ? 'selected' : ''} ${
          isEditing ? 'editing' : ''
        }`}
      >
        {isEditing ? (
          <input
            type="text"
            value={value}
            onChange={(e) => handleCellChange(e, row, col)}
            onBlur={handleBlur}
            onKeyDown={(e) => handleKeyDown(e, row, col)}
            autoFocus
            style={{width: "10rem"}}
          />
        ) : (
          <span className="text-white">{value.toUpperCase()}</span>
        )}
      </td>
    );
  };

  // END LOGIC FUNCTIONS

  return (
    <>
      <h2 className='px-4 pt-4 fw-bold text-warning'>EDITING RECORD #{reportId.toString().padStart(4, '0')}</h2>
      <p className='px-5'>Input the updated information for the income statement of certain period. Always double check before saving to ensure the accuracy of the record.</p>
      <div className='bg-light mx-5 rounded-3'>
        <Form onSubmit={handleUpdate}>
        <Container className='p-3 border-bottom d-flex gap-3 align-items-center'>
        <span className='fs-5'>STATEMENT REPORTING PERIOD:</span>
        <Row className='flex-fill'>
            <Col xs={4} className=''>
              <Form.Group className="" controlId="exampleForm.ControlInput1">
                <span className=''>Starting Date<span className='text-danger'>*</span>:</span>
                <Form.Control
                  onChange={(e) => setStartDate(e.target.value)}
                  value={startDate} 
                  name='startDate' 
                  type="date" 
                  required 
                  className='d-inline-block fw-bold'
                />
              </Form.Group>
            </Col>
            <Col xs={4} className=''>
              <Form.Group className="" controlId="exampleForm.ControlInput1">
                <span className=''>End Date<span className='text-danger'>*</span>:</span>
                <Form.Control
                  onChange={(e) => setEndDate(e.target.value)}
                  value={endDate} 
                  name='startDate' 
                  type="date" 
                  required 
                  className='d-inline-block fw-bold'
                />
              </Form.Group>
            </Col>
          </Row>
          <Button as={Link} to={'/all-records'} variant='warning' size='sm'><i class="fa-solid fa-xmark"></i> Cancel Editing</Button>
        </Container>
          <Container fluid className='p-3'>
            <Row className=''>
               {/* ===================== START REVENUE COLUMN ===================== */}
              <Col xs={4} className=''>
                {/* SERVICE INCOME */}
                <h4 className='text-center fw-bold'>REVENUE</h4>
                {/* Date */}
                
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>SERVICE INCOME:</Form.Label>
                  <InputGroup>
                    <InputGroup.Text>₱</InputGroup.Text>
                    <Form.Control 
                      onChange={(e) => {
                        setServiceIncome(e.target.value)
                      }} 
                      value={serviceIncome == 0 ? "" : serviceIncome}
                      name='serviceIncome' 
                      type="number" 
                      placeholder="0"
                    />
                  </InputGroup>
                </Form.Group>

                {/* SALES INCOME */}
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>SALES INCOME:</Form.Label>
                  <InputGroup>
                    <InputGroup.Text>₱</InputGroup.Text>
                    <Form.Control 
                      onChange={(e) => {
                        setSalesIncome(e.target.value)
                      }}  
                      value={salesIncome == 0 ? "" : salesIncome}
                      name='salesIncome' 
                      type="number" 
                      placeholder="0"
                    />
                  </InputGroup>
                </Form.Group>

                {/* OTHER INCOME */}
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>OTHER INCOME:</Form.Label>
                  <InputGroup>
                    <InputGroup.Text>₱</InputGroup.Text>
                    <Form.Control 
                      onChange={(e) => {
                        setOtherIncome(e.target.value)
                      }}  
                      value={otherIncome == 0 ? "" : otherIncome}
                      name='otherIncome' 
                      type="number" 
                      placeholder="0"
                    />
                  </InputGroup>
                </Form.Group>

                {/* FROM JOB ORDER */}
                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>FROM JOB ORDER/S:</Form.Label>
                    <Form.Control 
                      onChange={(e) => {
                        setJobOrders(e.target.value)
                      }}  
                      value={jobOrders}
                      name='jobOrders' 
                      type="text" 
                      placeholder="separate by comma e.g. 2094, 2233"
                    />
                </Form.Group>

              </Col>
              {/* ===================== END REVENUE COLUMN ===================== */}

              {/* ===================== START EXPENSES COLUMN ===================== */}
              <Col xs={5} className=''>

                <h4 className='text-center pb-3 fw-bold'>OPERATING EXPENSES AND OTHER DEDUCTIONS</h4>
                <div>
                  <p className='mb-0'>Click on the cells to add data.</p>
                  <Table id="spreadsheet">
                    <thead>
                      {data[0].map((header, col) => (
                        <th key={`header-${col}`}>{header}</th>
                      ))}
                    </thead>
                    <tbody>
                      {data.slice(1).map((rowData, row) => (
                        <tr key={`row-${row}`} className='table-warning'>
                          {rowData.map((cellData, col) =>
                            renderCell(row + 1, col, cellData)
                          )}
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                  <div className='text-end mb-5'>
                    <Button variant='dark' size='sm' onClick={handleAddRow}><i class="fa-solid fa-plus"></i> Item</Button>
                  </div>
                </div>
                
              </Col>

              {/* ===================== END EXPENSES COLUMN ===================== */}

              {/* ===================== START SUMMARY COLUMN ===================== */}
              <Col xs={3} className=''>
                <h4 className='text-center fw-bold'>SUMMARY</h4>
                <div>
                  {/* TOTAL REVENUE */}
                  <div className=''>
                    <h5 className='pt-3 mb-0'>TOTAL REVENUE:</h5>
                    <span className='bg-info text-light d-block ps-2 fs-3'>₱ {revenue.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
                  </div>
                  {/* TOTAL EXPENSES */}
                  <div className=''>
                    <div className='ms-auto d'>
                      <h5 className='pt-2 mb-0'>TOTAL EXPENSES:</h5>
                      <span className='bg-danger text-light d-block ps-2 fs-3'>₱ {expenses.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
                    </div>
                  </div>
                  {/* PROFIT */}
                  <div className='mb-3'>
                    <h5 className='pt-2 mb-0'>NET INCOME:</h5>
                    <span className='bg-success text-light d-block ps-2 fs-3'>₱ {netIncome.toLocaleString('en', {minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
                  </div>
                  <h5 className='pt-3 mb-0'>Remarks<span className='text-danger'>*</span>:</h5>
                  <Form.Control 
                      onChange={(e) => {
                        setRemarks(e.target.value)
                      }}  
                      value={remarks}
                      name='remarks' 
                      as="textarea"
                      rows={6}
                      required
                    />
                  <div className='text-center mt-3'>
                    <Button type='submit' variant='success' className='fs-4 hover-up'><i class="fa-solid fa-arrow-up-from-bracket"></i> UPDATE RECORD</Button>
                  </div>
                
                </div>          
              </Col>
              {/* ===================== END SUMMARY COLUMN ===================== */}
            </Row>
          </Container>
        </Form>
      </div>
    </>
  )
}

export default EditRecord