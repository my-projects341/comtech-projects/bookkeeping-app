import React from 'react'
import {Navbar, Container, Nav} from 'react-bootstrap'
import { Link } from 'react-router-dom'

const NavigationBar = () => {
  return (
    <Navbar className="bg-success">
        <Container fluid>
          <Navbar.Brand as={Link} to={"/"} className='text-white fw-bold'>
            COMTECH ACCOUNTING
          </Navbar.Brand>
          <Nav className="ms-auto">
            <Nav.Link  href="/" className='text-white'>J.O.M.</Nav.Link>
            <Nav.Link target='_blank' href="https://www.comtechgingoog.com/redirect-1.html" className='text-white'>J.O.TRACKER</Nav.Link>
            <Nav.Link target='_blank' href="https://comtechgingoog.com" className='me-lg-2 text-white'>WEBSITE</Nav.Link>
            <Nav.Link href="/logout" className='text-white bg-dark rounded-5 py-1 px-3 align-self-center'>Logout</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
  )
}

export default NavigationBar