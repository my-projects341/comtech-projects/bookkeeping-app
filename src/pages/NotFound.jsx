import React from 'react'
import {Button} from 'react-bootstrap'

const NotFound = () => {
  
  const handleGoBack = () => {
    window.history.back();
  };

  return (
    <div className='mt-5 text-center'>
        <h1 className='text-danger display-1 text-center mb-4'><i class="fa-regular fa-face-frown"></i></h1>
        <h3 className='text-danger text-center mb-3'>Page Not Found!</h3>
        <Button onClick={handleGoBack}>Go back</Button>
    </div>
  )
}

export default NotFound